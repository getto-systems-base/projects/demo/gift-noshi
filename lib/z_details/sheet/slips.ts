import { env } from "../../y_environment/env"
import { PaperSize, Workbook } from "exceljs"

type Slip = Readonly<{
    size: SlipSize
    type: string
    name: string
}>

type SlipSize = "A3" | "A4"

type HeightMap = Readonly<{
    type: number
    padding: number
    name: number
}>

interface PrintSlips {
    (slips: Slip[]): Promise<string>
}
export function newSheet_deliverySlips(): PrintSlips {
    return async (slips) => {
        const mock = false
        if (mock) {
            return `/${env.version}/noshi.xlsx`
        }
        const workbook = new Workbook()

        slips.forEach((slip, i) => {
            addSheet(workbook, slip, i+1)
        })

        const buffer = await workbook.xlsx.writeBuffer()
        return URL.createObjectURL(
            new Blob([buffer], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
            }),
        )
    }

    function addSheet(workbook: Workbook, slip: Slip, index: number) {
        switch (slip.size) {
            case "A3":
                sheet_A3(workbook, slip, index)
                return

            case "A4":
                sheet_A4(workbook, slip, index)
                return
        }
    }
    function sheet_A4(workbook: Workbook, slip: Slip, index: number) {
        const sheet = workbook.addWorksheet(`sheet-${index}`)

        sheet.properties.defaultRowHeight = BASE_HEIGHT

        sheet.pageSetup.orientation = "landscape"
        sheet.pageSetup.paperSize = PaperSize.A4
        sheet.pageSetup.horizontalDpi = 1200
        sheet.pageSetup.verticalDpi = 1200
        sheet.pageSetup.horizontalCentered = true
        sheet.pageSetup.margins = {
            top: 0.25,
            bottom: 0.25,
            left: 0,
            right: 0,
            header: 0,
            footer: 0,
        }
        sheet.pageSetup.blackAndWhite = true
        sheet.pageSetup.fitToPage = true
        sheet.pageSetup.fitToWidth = 1
        sheet.pageSetup.fitToHeight = 3

        const column = sheet.getColumn("A")
        column.width = 13
        column.font = {
            name: "HG正楷書体-PRO",
            family: 1,
            size: 60,
            bold: true,
        }
        column.alignment = {
            horizontal: "center",
            vertical: "middle",
            textRotation: "vertical",
            shrinkToFit: true,
        }

        const height: HeightMap = {
            type: 15,
            padding: 6,
            name: 22,
        }

        const type = sheet.addRow([slip.type])
        type.height = rowHeight(height.type)

        const padding = sheet.addRow([""])
        padding.height = rowHeight(height.padding)

        const name = sheet.addRow([slip.name])
        name.height = rowHeight(height.name)

        name.addPageBreak()
    }
    function sheet_A3(workbook: Workbook, slip: Slip, index: number) {
        const sheet = workbook.addWorksheet(`sheet-${index}`)

        sheet.properties.defaultRowHeight = BASE_HEIGHT

        sheet.pageSetup.orientation = "landscape"
        sheet.pageSetup.paperSize = 8 // A3
        sheet.pageSetup.horizontalDpi = 1200
        sheet.pageSetup.verticalDpi = 1200
        sheet.pageSetup.horizontalCentered = true
        sheet.pageSetup.margins = {
            top: 0.75,
            bottom: 0.75,
            left: 0,
            right: 0,
            header: 0,
            footer: 0,
        }
        sheet.pageSetup.blackAndWhite = true
        sheet.pageSetup.fitToWidth = 1

        const column = sheet.getColumn("A")
        column.width = 16.88
        column.font = {
            name: "HG正楷書体-PRO",
            family: 1,
            size: 75,
            bold: true,
        }
        column.alignment = {
            horizontal: "center",
            vertical: "middle",
            textRotation: "vertical",
            shrinkToFit: true,
        }

        const height: HeightMap = {
            type: 22,
            padding: 8,
            name: 26,
        }

        const type = sheet.addRow([slip.type])
        type.height = rowHeight(height.type)

        const padding = sheet.addRow([""])
        padding.height = rowHeight(height.padding)

        const name = sheet.addRow([slip.name])
        name.height = rowHeight(height.name)

        name.addPageBreak()
    }
}

function rowHeight(height: number): number {
    return height * BASE_HEIGHT
}
const BASE_HEIGHT = 13.5
